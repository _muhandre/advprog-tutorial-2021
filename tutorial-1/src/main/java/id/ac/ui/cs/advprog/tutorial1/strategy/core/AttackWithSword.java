package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    public AttackWithSword(){}

    @Override
    public String attack() {
        return "Starburst Stream";
    }

    @Override
    public String getType() {
        return "Pedang";
    }
}
