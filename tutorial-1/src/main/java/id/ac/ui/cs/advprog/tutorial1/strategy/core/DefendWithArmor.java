package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    public DefendWithArmor(){}

    @Override
    public String defend() {
        return "Armored";
    }

    @Override
    public String getType() {
        return "Armor";
    }
}
