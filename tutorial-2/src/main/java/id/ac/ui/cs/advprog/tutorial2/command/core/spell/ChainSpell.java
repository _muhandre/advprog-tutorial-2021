package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private ArrayList<Spell> spells;
    private boolean hasBeenUndo = false;

    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }


    @Override
    public void cast() {
        for(Spell spell : spells) {
            spell.cast();
        }
        hasBeenUndo = false;
    }

    @Override
    public void undo() {
        if(hasBeenUndo)
            return;
        for(Spell spell : spells) {
            spell.undo();
        }
        hasBeenUndo = true;
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
