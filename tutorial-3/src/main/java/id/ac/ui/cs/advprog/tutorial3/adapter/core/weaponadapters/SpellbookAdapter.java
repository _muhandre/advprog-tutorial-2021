package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean latestAttackIsChargedAttack;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        latestAttackIsChargedAttack = false;
    }

    @Override
    public String normalAttack() {
        latestAttackIsChargedAttack = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if(latestAttackIsChargedAttack) {
            latestAttackIsChargedAttack = false;
            return "Magic power not enough for large spell";
        }
        latestAttackIsChargedAttack = true;
        return spellbook.largeSpell();
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
