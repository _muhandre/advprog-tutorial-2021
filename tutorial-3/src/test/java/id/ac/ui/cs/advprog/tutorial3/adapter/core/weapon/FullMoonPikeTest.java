package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

// TODO: add tests
public class FullMoonPikeTest {
    private Class<?> fullMoonPikeClass;
    private FullMoonPike objectForTest;

    @BeforeEach
    public void setUp() throws Exception {
        fullMoonPikeClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.FullMoonPike");
        objectForTest = new FullMoonPike("Person Name");
    }

    @Test
    public void testFullMoonPikeIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(fullMoonPikeClass.getModifiers()));
    }

    @Test
    public void testFullMoonPikeIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(fullMoonPikeClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon")));
    }

    @Test
    public void testFullMoonPikeOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = fullMoonPikeClass.getDeclaredMethod("normalAttack");

        assertEquals("java.lang.String",
                normalAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    public void testFullMoonPikeOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = fullMoonPikeClass.getDeclaredMethod("chargedAttack");

        assertEquals("java.lang.String",
                chargedAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    public void testFullMoonPikeOverrideGetNameMethod() throws Exception {
        Method getName = fullMoonPikeClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testFullMoonPikeOverrideGetHolderMethod() throws Exception {
        Method getHolderName = fullMoonPikeClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    // TODO: buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testFullMoonPikeNormalAttackMethod() throws Exception {
        String log = objectForTest.normalAttack();
        assertEquals("Moonlight Op. 27 No. 2", log);
    }

    @Test
    public void testFullMoonPikeChargedAttackMethod() throws Exception {
        String log = objectForTest.chargedAttack();
        assertEquals("Moonlight Sonata III Presto Agitato", log);
    }

    @Test
    public void testFullMoonPikeGetNameMethod() throws Exception {
        String objectName = objectForTest.getName();
        assertEquals("Full Moon Pike", objectName);
    }

    @Test
    public void testFullMoonPikeGetHolderNameMethod() throws Exception {
        String holderName = objectForTest.getHolderName();
        assertEquals("Person Name", holderName);
    }
}
