package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Heatbearer;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

// TODO: add tests
public class SpellbookAdapterTest {
    private Class<?> spellbookAdapterClass;
    private Class<?> spellbookClass;
    private Spellbook objectForTest;
    private SpellbookAdapter adapterForTest;

    @BeforeEach
    public void setUp() throws Exception {
        spellbookAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter");
        spellbookClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook");
        objectForTest = new Heatbearer("Person Name");
        adapterForTest = new SpellbookAdapter(objectForTest);
    }

    @Test
    public void testSpellbookAdapterIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(spellbookAdapterClass.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(spellbookAdapterClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon")));
    }

    @Test
    public void testSpellbookAdapterConstructorReceivesSpellbookAsParameter() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = spellbookClass;
        Collection<Constructor<?>> constructors = Arrays.asList(
                spellbookAdapterClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    public void testSpellbookAdapterOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = spellbookAdapterClass.getDeclaredMethod("normalAttack");

        assertEquals("java.lang.String",
                normalAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = spellbookAdapterClass.getDeclaredMethod("chargedAttack");

        assertEquals("java.lang.String",
                chargedAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideGetNameMethod() throws Exception {
        Method getName = spellbookAdapterClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideGetHolderMethod() throws Exception {
        Method getHolderName = spellbookAdapterClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    // TODO: buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testSpellbookAdapterNormalAttackMethod() throws Exception {
        String objectLog = objectForTest.smallSpell();
        String adapterLog = adapterForTest.normalAttack();

        assertEquals(objectLog, adapterLog);
    }

    @Test
    public void testSpellbookAdapterChargedAttackMethodOnce() throws Exception {
        String objectLog = objectForTest.largeSpell();
        String adapterLog = adapterForTest.chargedAttack();

        assertEquals(objectLog, adapterLog);
    }

    @Test
    public void testSpellbookAdapterChargedAttackMethodTwice() throws Exception {
        adapterForTest.chargedAttack();
        String adapterLog = adapterForTest.chargedAttack();

        assertEquals("Magic power not enough for large spell", adapterLog);
    }

    @Test
    public void testSpellbookAdapterChargedAttackMethodThreeTimes() throws Exception {
        String objectLog = objectForTest.largeSpell();
        adapterForTest.chargedAttack();
        adapterForTest.chargedAttack();
        String adapterLog = adapterForTest.chargedAttack();

        assertEquals(objectLog, adapterLog);
    }

    @Test
    public void testSpellbookAdapterGetNameMethod() throws Exception {
        String objectName = objectForTest.getName();
        String adapterName = adapterForTest.getName();

        assertEquals(objectName, adapterName);
    }

    @Test
    public void testSpellbookAdapterGetHolderNameMethod() throws Exception {
        String firstName = objectForTest.getHolderName();
        String secondName = adapterForTest.getHolderName();

        assertEquals(firstName, secondName);
    }
}
