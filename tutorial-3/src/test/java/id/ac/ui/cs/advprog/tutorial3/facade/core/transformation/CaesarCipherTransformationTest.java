package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CaesarCipherTransformationTest {
    private Class<?> caesarCipherClass;

    @BeforeEach
    public void setup() throws Exception {
        caesarCipherClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarCipherTransformation");
    }

    @Test
    public void testCaesarCipherHasEncodeMethod() throws Exception {
        Method translate = caesarCipherClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCaesarCipherEncodesCorrectly() throws Exception {
        String text = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "EFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123";

        Spell result = new CaesarCipherTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCaesarCipherEncodesCorrectlyWithCustomShift() throws Exception {
        String text = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "DEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012";

        Spell result = new CaesarCipherTransformation(3).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCaesarCipherHasDecodeMethod() throws Exception {
        Method translate = caesarCipherClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCaesarCipherDecodesCorrectly() throws Exception {
        String text = "EFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        Spell result = new CaesarCipherTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCaesarCipherDecodesCorrectlyWithCustomShift() throws Exception {
        String text = "DEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        Spell result = new CaesarCipherTransformation(3).decode(spell);
        assertEquals(expected, result.getText());
    }
}
