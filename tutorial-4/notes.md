# Singleton Pattern Approach

## Lazy Instantiation

### Cara kerja
Instansiasi *object* singleton tidak dilakukan begitu saat program dijalankan, hanya ketika *object* tersebut akan digunakan.

### Keuntungan
1. *Memory space* digunakan secara efisien.

### Kekurangan
1. Terdapat selang waktu apabila ada suatu *object* yang perlu digunakan sedari awal.
2. Memerlukan sinkronisasi untuk aplikasi dengan arsitektur *multithreading*.

## Eager Instantiation

### Cara kerja
Instansiasi *object* singleton akan dilakukan begitu saat program dijalankan. Dengan begitu, ketika ada permintaan untuk mendapatkan *object* tersebut cukup dengan me-*return* *object* tersebut.

### Keuntungan
1. Eksekusi program dapat lebih cepat apabila ada suatu *object* singleton yang digunakan oleh program sedari awal.
2. Tidak perlu ada proses *sinkronisasi* untuk aplikasi dengan arsitektur *multithreading*

### Kekurangan
1. *Memory space* tidak digunakan secara efisien karena ada akan ada *object* singleton yang bisa jadi tidak digunakan namun di instansiasi.
