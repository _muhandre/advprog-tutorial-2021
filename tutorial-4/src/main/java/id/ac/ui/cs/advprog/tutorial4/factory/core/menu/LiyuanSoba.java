package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.LSIngridientFactory;

public class LiyuanSoba extends Menu {
    //Ingridients:
    //Noodle: Soba
    //Meat: Beef
    //Topping: Mushroom
    //Flavor: Sweet
    public LiyuanSoba(String name){
        super(name, new LSIngridientFactory());
    }
}