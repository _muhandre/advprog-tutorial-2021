package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MenuRepositoryTest {
    private MenuRepository menuRepository;

    @Mock
    private List<Menu> listMenu;

    @BeforeEach
    public void setUp() {
        menuRepository = new MenuRepository();
        listMenu = new ArrayList<>();
    }

    @Test
    public void whenMenuRepoAddMethodItShouldAddMenuToList() {
        ReflectionTestUtils.setField(menuRepository, "list", listMenu);
        Menu acquiredMenu = new InuzumaRamen("Ramen Inuzuma");
        menuRepository.add(acquiredMenu);
        assertThat(listMenu.get(0)).isEqualTo(acquiredMenu);
    }

    @Test
    public void whenMenuRepoGetMenusMethodItShouldReturnListOfMenu() {
        ReflectionTestUtils.setField(menuRepository, "list", listMenu);
        List<Menu> acquiredListOfMenu = menuRepository.getMenus();
        assertThat(acquiredListOfMenu).isEqualTo(listMenu);
    }
}
