package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuServiceImplTest {
    private Class<?> menuServiceClass;

    @InjectMocks
    private MenuServiceImpl menuService;

    @BeforeEach
    public void setup() throws Exception {
        menuServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl");
        menuService = new MenuServiceImpl();
    }

    @Test
    public void testMenuServiceGetMenusMethodCorrectlyImplemented() throws Exception {
        assertEquals(menuService.getMenus().size(), 4);
    }

    @Test
    public void testMenuServiceCreateMenuCorrectlyImplemented() {
        Menu acquiredMenu = menuService.createMenu("Soba baru", "LiyuanSoba");
        assertEquals(menuService.getMenus().get(4), acquiredMenu);
    }
}
