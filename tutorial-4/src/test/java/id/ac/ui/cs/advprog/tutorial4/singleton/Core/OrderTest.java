package id.ac.ui.cs.advprog.tutorial4.singleton.Core;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;


import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OrderTest {

    private Class<?> orderDrinkClass;
    private OrderDrink orderDrink;

    private Class<?> orderFoodClass;
    private OrderFood orderFood;

    @BeforeEach
    public void setUp() throws Exception {
        orderDrinkClass = Class.forName(OrderDrink.class.getName());
        orderDrink = OrderDrink.getInstance();

        orderFoodClass = Class.forName(OrderFood.class.getName());
        orderFood = OrderFood.getInstance();
    }

    @Test
    public void testOrderDrinkIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(orderDrinkClass.getModifiers()));
    }

    @Test
    public void testOrderFoodIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(orderFoodClass.getModifiers()));
    }


    @Test
    public void testNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(orderDrinkClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertEquals(false, check);
    }

    @Test
    public void testNoPublicConstructors2() {
        List<Constructor> constructors = Arrays.asList(orderFoodClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertEquals(false, check);
    }


    @Test
    public void testGetInstanceShouldReturnSingletonInstance() {
        OrderDrink orderDrink = OrderDrink.getInstance();

        assertNotNull(orderDrink);
    }

    @Test
    public void testGetInstanceShouldReturnSingletonInstance2() {
        OrderFood orderFood = OrderFood.getInstance();

        assertNotNull(orderFood);
    }

    @Test
    public void testOrderDrink(){
        orderDrink.setDrink("Bir Bintang");
        assertEquals("Bir Bintang", orderDrink.getDrink());
    }

    @Test
    public void testOrderFood(){
        orderFood.setFood("Tonkotsu Ramen");
        assertEquals("Tonkotsu Ramen", orderFood.getFood());
    }

    @Test
    public void testOrderDrinkExist(){
        assertNotNull(orderDrink.toString());
    }

    @Test
    public void testOrderFoodExist(){
        assertNotNull(orderFood.toString());

    }

    @Test
    public void testOrderFoodApplySingletonPrinciple() {
        assertTrue(orderFood == OrderFood.getInstance());
    }

    @Test
    public void testOrderDrinkApplySingletonPrinciple() {
        assertTrue(orderDrink == OrderDrink.getInstance());
    }
}
