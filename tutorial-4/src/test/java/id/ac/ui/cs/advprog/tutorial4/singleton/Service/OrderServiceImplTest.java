package id.ac.ui.cs.advprog.tutorial4.singleton.Service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class OrderServiceImplTest {
    private Class<?> orderServiceClass;

    @InjectMocks
    private OrderServiceImpl orderService;

    @BeforeEach
    public void setup() throws Exception {
        orderServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
    }

    @Test
    public void testOrderServiceOrderADrinkMethod() throws Exception {
        Method orderADrink = orderServiceClass.getDeclaredMethod("orderADrink", String.class);
        int methodModifiers = orderADrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderServiceOrderADrinkCorrectlyImplemented() {
        orderService.getDrink();
        orderService.orderADrink("minuman");
        assertEquals(orderService.getDrink().toString(), "minuman");
    }

    @Test
    public void testOrderServiceOrderAFoodMethod() throws Exception {
        Method orderAFood = orderServiceClass.getDeclaredMethod("orderAFood", String.class);
        int methodModifiers = orderAFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderServiceOrderAFoodCorrectlyImplemented() {
        orderService.orderAFood("makanan");
        assertEquals(orderService.getFood().toString(), "makanan");
    }
}
